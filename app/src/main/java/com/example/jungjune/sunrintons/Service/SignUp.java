package com.example.jungjune.sunrintons.Service;

/**
 * Created by jungjune on 2016-07-12.
 */
public class SignUp {
    String user_id;
    String user_pw;
    int user_grade;
    int user_schoolClass;
    int user_number;

    public String getUser_id(String user_id){
        return user_id;
    }
    public String getUser_pw(String user_pw){
        return user_pw;
    }
    public int getUser_grade(int user_grade){
        return user_grade;
    }
    public int getUser_schoolClass(int user_schoolClass){
        return user_schoolClass;
    }
    public int getUser_number(int user_number){
        return user_number;
    }

    public void setUser_id(String user_id){
        this.user_id=user_id;
    }

    public void setUser_pw(String user_pw) {
        this.user_pw = user_pw;
    }

    public void setUser_grade(int user_grade) {
        this.user_grade = user_grade;
    }

    public void setUser_schoolClass(int user_schoolClass) {
        this.user_schoolClass = user_schoolClass;
    }

    public void setUser_number(int user_number) {
        this.user_number = user_number;
    }
}
