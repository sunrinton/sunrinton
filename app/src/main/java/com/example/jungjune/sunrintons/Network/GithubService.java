package com.example.jungjune.sunrintons.Network;

import com.example.jungjune.sunrintons.Service.Login;
import com.example.jungjune.sunrintons.Service.SignUp;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.POST;

/**
 * Created by jungjune on 2016-07-22.
 */
public interface GithubService {
    @POST("/login")
    Call<List<Login>> Login(@Field("id") String id, @Field("pw") String pw);
    @POST("/signin")
    Call<List<SignUp>>signup(@Field("id") String id, @Field("pw")String pw, @Field("class")int classes, @Field("grade")int grade, @Field("number")int number);

}
