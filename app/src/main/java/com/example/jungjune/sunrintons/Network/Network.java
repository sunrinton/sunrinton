package com.example.jungjune.sunrintons.Network;

import retrofit2.Retrofit;

/**
 * Created by jungjune on 2016-07-22.
 */
public class Network {
    public GithubService getService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://malang.moe:6000")
                .build();

        GithubService service = retrofit.create(GithubService.class);
        return service;
    }
}
